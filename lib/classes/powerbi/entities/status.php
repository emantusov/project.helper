<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 18:13
 */

namespace Project\Helper\Classes\PowerBI\Entities;


class Status extends BaseBiEntity
{
    private $id, $statusId, $name, $entityId;

    /**
     * User constructor.
     * @param $fields
     */
    public function __construct($fields)
    {
        $this->id = $fields['ID'];
        $this->statusId = $fields['STATUS_ID'];
        $this->name = $fields['NAME'];
        $this->entityId = $fields['ENTITY_ID'];
        parent::__construct($fields);
    }

    /**
     * @return array
     */
    public function toBiArray()
    {
        $fields = [
            'entity_id' => $this->id,
            'name' => $this->name,
            'status_id' => $this->statusId,
            'entity_id' => $this->entityId,
        ];
        return $fields;
    }
}