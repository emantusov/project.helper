<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 19:08
 */

namespace Project\Helper\Classes\PowerBI\Entities;


class Client extends BaseBiEntity
{
    private $id, $name, $lastName, $secondName, $dateCreated, $assignerById, $creatorById, $modifyById, $sourceId,
        $roistat, $amo, $ratingShit, $rating, $smsSubscribe, $mailSubscribe, $site, $blackList, $segment, $cityId,
        $negative, $typeId, $unisender, $contactArchiv, $campaign, $createMonth, $createWeek, $promo17, $abonementDate,
        $pasport, $leadId, $address, $comments, $sourceDescription;

    /**
     * Client constructor.
     * @param $fields
     */
    public function __construct($fields)
    {
        $this->id = $fields['ID'];
        $this->name = $fields['NAME'];
        $this->lastName = $fields['LAST_NAME'];
        $this->secondName = $fields['SECOND_NAME'];
        $this->dateCreated = $fields['DATE_CREATE'];
        $this->assignerById = $fields['ASSIGNED_BY_ID'];
        $this->creatorById = $fields['CREATED_BY_ID'];
        $this->modifyById = $fields['MODIFY_BY_ID'];
        $this->sourceId = $fields['SOURCE_ID'];
        $this->roistat = $fields['UF_ROISTAT'];
        $this->amo = $fields['UF_AMO'];
        $this->ratingShit = $fields['UF_RAITING_CHIT'];
        $this->rating = $fields['UF_RAITING'];
        $this->smsSubscribe = $fields['UF_SMS_SUBS'];
        $this->mailSubscribe = $fields['UF_MAIL_SUBS'];
        $this->site = $fields['UF_SITE'];
        $this->blackList = $fields['UF_BLACK_LIST'];
        $this->segment = $fields['UF_SEGMENT'];
        $this->cityId = $fields['UF_CITY'];
        $this->negative = $fields['UF_NEGATIVE'];
        $this->typeId = $fields['TYPE_ID'];
        $this->unisender = $fields['UF_UNISENDER'];
        $this->contactArchiv = $fields ['UF_CONTACT_ARCHIV'];
        $this->campaign = $fields ['UF_CAMPAIGN'];
        $this->createMonth = $fields['DATE_CREATE'];
        $this->createWeek = $fields['DATE_CREATE'];
        $this->promo17 = $fields['UF_PROMO_17'];
        $this->abonementDate = $fields['UF_ABONEMENT_DATE'];
        $this->pasport = $fields['UF_PASPORT'];
        $this->leadId = $fields['LEAD_ID'];
        $this->address = $fields['ADDRESS'];
        $this->comments = $fields['COMMENTS'];
        $this->sourceDescription = $fields['SOURCE_DESCRIPTION'];
        parent::__construct($fields);
    }

    /**
     * @return array
     */
    public function toBiArray()
    {
        $fields = [
            'contact_id' => $this->id,
            'contact_name' => $this->name,
            'contact_last_name' => $this->lastName,
            'contact_second_name' => $this->secondName,
            'date_created' => $this->dateCreated,
            'contact_assigned_by_id' => $this->assignerById,
            'contact_modified_by_id' => $this->modifyById,
            'uf_raiting_chit' => $this->ratingShit,
            'contact_source_id' => $this->sourceId,
            'uf_roistat' => $this->roistat,
            'uf_amo' => $this->amo,
            'uf_raiting' => $this->rating,
            'uf_mail_subs' => $this->mailSubscribe,
            'uf_site' => $this->site,
            'uf_sms_subs' => $this->smsSubscribe,
            'uf_black_list' => $this->blackList,
            'uf_segment' => $this->segment,
            'uf_city' => $this->cityId,
            'uf_unisender' => $this->unisender,
            'uf_contact_atchiv' => $this->contactArchiv,
            'created_month' => $this->createMonth,
            'created_week' => $this->createWeek,
            'contact_type_id' => $this->typeId,
            'uf_negative' => $this->negative,
            'uf_campaign' => $this->campaign,
            'created_by_id' => $this->creatorById,
            'uf_segment' => $this->segment,
            'uf_promo_17' => $this->promo17,
            'uf_abonement_date' => $this->abonementDate,
            'uf_pasport' => $this->pasport,
            'lead_id' => $this->leadId,
            'contact_address' => $this->address,
            'contact_comments' => $this->comments,
            'contact_description' => $this->sourceDescription,
            'entity_id' => $this->id,

        ];
        return $fields;
    }

}