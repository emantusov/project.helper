<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 18:13
 */

namespace Project\Helper\Classes\PowerBI\Entities;


class Zone extends BaseBiEntity
{
    private $id, $xmlId, $price = 0, $currency = 'RUB', $name, $entityId;

    /**
     * User constructor.
     * @param $fields
     */
    public function __construct($fields)
    {
        $this->id = $fields['ID'];
        $this->xmlId = $fields['ID'];
        $this->name = $fields['NAME'];
        $this->entityId = $fields['ID'];
        parent::__construct($fields);

    }

    /**
     * @return array
     */
    public function toBiArray()
    {
        $fields = [
            'sku_id' => $this->id,
            'sku_xml_id' => $this->xmlId,
            'sku_price' => $this->price,
            'sku_currency' => $this->currency,
            'zone_name' => $this->name,
            'entity_id' => $this->entityId,
        ];
        return $fields;
    }
}