<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 18:13
 */

namespace Project\Helper\Classes\PowerBI\Entities;


class City extends BaseBiEntity
{
    private $id, $name, $code;

    /**
     * User constructor.
     * @param $fields
     */
    public function __construct($fields)
    {
        $this->id = $fields['ID'];
        $this->name = $fields['NAME'];
        $this->code = $fields['CODE'];
        parent::__construct($fields);
    }

    /**
     * @return array
     */
    public function toBiArray()
    {
        $fields = [
            'city_id' => $this->id,
            'code' => $this->code,
            'city_name' => $this->name,
            'entity_id' => $this->id,
        ];
        return $fields;
    }
}