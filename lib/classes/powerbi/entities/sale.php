<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 19:08
 */

namespace Project\Helper\Classes\PowerBI\Entities;


use Bitrix\Main\ArgumentException;
use Bitrix\Main\Web\Json;

class Sale extends BaseBiEntity
{
    private $id, $deadline, $createdDate, $responsibleId, $ufOperator, $ufCity, $ufType, $ufStageId,
        $ufOpportunity, $ufNoPrepayment, $ufHowWasIt, $ufReaction, $ufZone, $udResultCons, $ufDatePay, $ufDateSurchargeN,
        $ufRefundDate, $ufDateSurchargeB, $ufSumN, $ufSurchargeB, $ufSumB, $ufRefundSumN, $ufSurchargeN, $ufSource,
        $ufRefundSumB, $ufOldDeal, $ufSegment, $ufSite, $ufSumR, $ufDateSurchargeB3, $ufDateSurchargeN3,
        $ufDateSurchargeN2, $ufSurchargeN3, $ufSurchargeR, $ufDateSurchargeB2, $ufDateSurchargeR, $ufSurchargeB3,
        $ufSurchargeN2, $ufSurchargeB2, $ufLanding, $contactId, $ufCrm, $raiting, $changeStudio;

    /**
     * Client constructor.
     * @param $fields
     */
    public function __construct($fields)
    {
        $this->id = $fields['ID'];
        $this->deadline = $fields['DEADLINE'];
        $this->createdDate = $fields['CREATED_DATE'];
        $this->responsibleId = $fields['RESPONSIBLE_ID'];
        $this->ufOperator = $fields['UF_OPERATOR'];
        $this->ufCity = $fields['UF_CITY'];
        $this->ufType = $fields['UF_TYPE'];
        $this->ufStageId = $fields['UF_STAGE_ID'];
        $this->ufOpportunity = $fields['UF_OPPORTUNITY'];
        $this->ufNoPrepayment = $fields['UF_NO_PREPAYMANTS'];
        $this->ufHowWasIt = $fields['UF_HOW_WAS_IT'];
        $this->ufReaction = $fields['UF_REACTION'];
        $this->ufZone = $fields['UF_ZONE'];
        $this->udResultCons = $fields['UF_RESULT_CONS'];
        $this->ufDatePay = $fields['UF_DATE_PAY'];
        $this->ufDateSurchargeN = $fields['UF_DATE_SURCHARGE_N'];
        $this->ufRefundDate = $fields['UF_REFUND_DATE'];
        $this->ufDateSurchargeB = $fields['UF_DATE_SURCHARGE_B'];
        $this->ufSumN = ($fields['UF_SUM_N']) ?: 0;
        $this->ufSumB = ($fields['UF_SUM_B']) ?: 0;
        $this->ufSumR = ($fields['UF_SUM_R']) ?: 0;
        $this->ufSurchargeB = ($fields['UF_SURCHARGE_B']) ?: 0;
        $this->ufSurchargeB2 = ($fields['UF_SURCHARGE_B2']) ?: 0;
        $this->ufSurchargeB3 = ($fields ['UF_SURCHARGE_B3']) ?: 0;
        $this->ufSurchargeN = ($fields['UF_SURCHARGE_N']) ?: 0;
        $this->ufSurchargeN2 = ($fields ['UF_SURCHARGE_N2']) ?: 0;
        $this->ufSurchargeN3 = ($fields ['UF_SURCHARGE_N3']) ?: 0;
        $this->ufSurchargeR = ($fields['UF_SURCHARGE_R']) ?: 0;
        $this->ufRefundSumN = $fields['UF_REFUND_SUM_N'];
        $this->ufSource = $fields ['UF_SOURCE'];
        $this->ufRefundSumB = $fields ['UF_REFUND_SUM_B'];
        $this->ufOldDeal = $fields['UF_OLD_DEAL'];
        $this->ufSegment = $fields['UF_SEGMENT'];
        $this->ufSite = $fields['UF_SITE'];
        $this->ufDateSurchargeB3 = $fields['UF_DATE_SURCHARGE_B3'];
        $this->ufDateSurchargeN3 = $fields['UF_DATE_SURCHARGE_N3'];
        $this->ufDateSurchargeN2 = $fields ['UF_DATE_SURCHARGE_N2'];
        $this->ufDateSurchargeB2 = $fields['UF_DATE_SURCHARGE_B2'];
        $this->ufDateSurchargeB3 = $fields['UF_DATE_SURCHARGE_B3'];
        $this->ufDateSurchargeR = $fields['UF_DATE_SURCHARGE_R'];
        $this->ufLanding = $fields['UF_LANDING'];
        $this->raiting = $fields['UF_RAITING'];
        $this->changeStudio = $fields['UF_CHANGE_STUDIO'];
        $this->contactId = str_replace('C_', ' ', $fields['UF_CRM_TASK'][0]);
        try {
            $this->ufCrm = Json::encode($fields['UF_CRM_TASK']);
        } catch (ArgumentException $e) {
            $this->ufCrm = '';
        }
        parent::__construct($fields);
    }

    /**
     * @return array
     */
    public function toBiArray()
    {
        $fields = [
            'uf_crm_task' => $this->ufCrm,
            'task_id' => $this->id,
            'deadline' => $this->deadline,
            'created_date' => $this->createdDate,
            'responsible_id' => $this->responsibleId,
            'uf_operator' => $this->ufOperator,
            'uf_city' => $this->ufCity,
            'uf_type' => $this->ufType,
            'uf_stage_id' => $this->ufStageId,
            'uf_opportunity' => $this->ufOpportunity,
            'uf_no_prepaymants' => $this->ufNoPrepayment,
            'uf_how_was_it' => $this->ufHowWasIt,
            'reaction' => $this->ufReaction,
            'uf_zone_id' => $this->ufZone,
            'uf_result_cons' => $this->udResultCons,
            'uf_date_pay' => $this->ufDatePay,
            'uf_date_surcharge_n' => $this->ufDateSurchargeN,
            'uf_refund_date' => $this->ufRefundDate,
            'uf_date_surcharge_b' => $this->ufDateSurchargeB,
            'uf_sum_n' => $this->ufSumN,
            'uf_sum_b' => $this->ufSumB,
            'uf_sum_r' => $this->ufSumR,
            'uf_surcharge_b' => $this->ufSurchargeB,
            'uf_surcharge_b2' => $this->ufSurchargeB2,
            'uf_surcharge_b3' => $this->ufSurchargeB3,
            'uf_surcharge_n' => $this->ufSurchargeN,
            'uf_surcharge_n2' => $this->ufSurchargeN2,
            'uf_surcharge_n3' => $this->ufSurchargeN3,
            'uf_surcharge_r' => $this->ufSurchargeR,
            'uf_refund_sum_n' => $this->ufRefundSumN,
            'uf_source' => $this->ufSource,
            'uf_refund_sum_b' => $this->ufRefundSumB,
            'uf_old_deal' => $this->ufOldDeal,
            'uf_segment' => $this->ufSegment,
            'uf_site' => $this->ufSite,
            'uf_date_surcharge_b3' => $this->ufDateSurchargeB3,
            'created_month' => $this->createdDate,
            'uf_date_surcharge_n3' => $this->ufDateSurchargeN3,
            'uf_date_surcharge_n2' => $this->ufDateSurchargeN2,
            'uf_date_surcharge_b2' => $this->ufDateSurchargeB2,
            'uf_date_surcharge_r' => $this->ufDateSurchargeR,
            'uf_landing' => $this->ufLanding,
            'contact_id' => $this->contactId,
            'uf_raiting' => $this->raiting,
            'uf_change_studio' => $this->changeStudio,
            'entity_id' => $this->id,
        ];
        return $fields;
    }
}