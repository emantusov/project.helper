<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 18:49
 */

namespace Project\Helper\Classes\PowerBI\Entities;


interface BiEntity
{
    public function __construct($fields);
    public function toBiArray();
    public function getEntityId();

}