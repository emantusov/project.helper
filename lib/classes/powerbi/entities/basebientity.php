<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 18:49
 */

namespace Project\Helper\Classes\PowerBI\Entities;


abstract class BaseBiEntity implements BiEntity
{
    private $id;
    public function __construct($fields){
        $this->id = $fields['ID'];
    }

    public function toBiArray()
    {
        // TODO: Implement toBiArray() method.
    }

    public function getEntityId(){
        return $this->id;
    }

}