<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 18:13
 */

namespace Project\Helper\Classes\PowerBI\Entities;

use CrmAPI;

class User extends BaseBiEntity
{
    private $id, $internalPhone, $mobilePhone, $name, $lastName, $mail, $entityId;

    /**
     * User constructor.
     * @param $fields
     */
    public function __construct($fields)
    {
        $this->id = $fields['ID'];
        $this->mail = $fields['PERSONAL_MAILBOX'];
        $this->internalPhone = $fields['UF_PHONE_INNER'];
        $this->mobilePhone = CrmAPI::checkPhone($fields['PERSONAL_PHONE']);
        $this->name = $fields['NAME'];
        $this->lastName = $fields['LAST_NAME'];
        $this->entityId = $fields['ID'];
        parent::__construct($fields);
    }

    /**
     * @return array
     */
    public function toBiArray()
    {
        $fields = [
            'user_id' => $this->id,
            'user_mail' => $this->mail,
            'user_internal_phone' => $this->internalPhone,
            'user_mobile' => $this->mobilePhone,
            'user_name' => $this->lastName . ' ' . $this->name,
            'entity_id' => $this->entityId,
        ];
        return $fields;
    }
}