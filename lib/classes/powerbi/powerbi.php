<?php /** @noinspection PhpUndefinedMethodInspection */

/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 15:06
 */

namespace Project\Helper\Classes\PowerBI;

use Bitrix\Main\ObjectException,
    Bitrix\Main\Diag\Debug;


class PowerBI
{
    /** Entities list
     *'Cities',
     *'Sales',
     *'Clients',
     *'Status',
     *'Users',
     *'Zones',
     */

    /**
     * @param $arProviders
     */
    public static function addAll($arProviders)
    {
        static::saveTimestamp();
        foreach ($arProviders as $providerName) {
            if (!$provider = static::getProvider($providerName)) {
                continue;
            }
            $query = $provider::getQuery();
            $arEntity = $provider::getEntities(false);
            foreach ($arEntity as $entity) {
                $query::add($entity->toBiArray());
            }
        }
    }

    /**
     * @param $offset
     * @param $providerName
     * @return int
     */
    public static function addByOffset($offset, $providerName)
    {
        $count = 0;
        if (!$provider = static::getProvider($providerName)) {
            return "provider don't find";
        }
        $query = $provider::getQuery();
        $arEntity = $provider::getEntitiesByOffset($offset);
        foreach ($arEntity as $entity) {
            $count++;
            $query::add($entity->toBiArray());
        }
        return $count;
    }

    /**
     * @param $arProviders
     */
    public static function UpdateAll($arProviders)
    {
        $dateFilter = static::getDateFilter();
        $arSelect = ['id'];
        foreach ($arProviders as $providerName) {
            if (!$provider = static::getProvider($providerName)) {
                continue;
            }
            $query = $provider::getQuery();
            $arEntity = $provider::getEntities($dateFilter);
            foreach ($arEntity as $entity) {
                $oldEntity = $query::query()->setFilter(['=entity_id' => $entity->getEntityId()])->
                setSelect($arSelect)->exec()->fetch();
                if ($oldEntity) {
                    $query::update($oldEntity['id'], $entity->toBiArray());
                } else {
                    $query::add($entity->toBiArray());
                }
            }
        }
    }

    /**
     * @param $name
     * @return bool|string
     */
    private static function getProvider($name)
    {
        try {
            $provider = static::checkProvider($name);
        } catch (ObjectException $e) {
            Debug::dumpToFile($e->getMessage(), 'provider', 'power_bi.log');
            echo($e->getMessage());
            return false;
        }
        return $provider;
    }

    /**
     * @param $name
     * @return string
     * @throws ObjectException
     */
    private static function checkProvider($name)
    {
        $class = 'Project\Helper\Classes\PowerBI\Providers\\' . $name . 'BiProvider';
        if (class_exists($class)) {
            return $class;
        } else {
            throw new ObjectException('not find ' . $class);
        }
    }

    /**
     * @return array
     */
    private static function getDateFilter()
    {
        $dateNow = static::getDateNow();
        $dateFrom = static::getTimestamp();
        $arDateFilter = [$dateFrom, $dateNow];
        static::saveTimestamp($dateNow);
        return $arDateFilter;
    }

    /**
     * @param bool $dateNow
     */
    private static function saveTimestamp($dateNow = false)
    {
        if (!$dateNow) {
            $dateNow = static::getDateNow();
        }
        \COption::SetOptionString('project.helper', 'timestamp', $dateNow);
    }

    /**
     * @return bool|null|string
     */
    private static function getTimestamp()
    {
        $dateFrom = \COption::GetOptionInt('project.helper', 'timestamp');
        return $dateFrom;
    }

    /**
     * @return string
     */
    private static function getDateNow()
    {
        $date = new \DateTime();
        $dateNow = $date->format('d.m.Y H:i:s');
        return $dateNow;
    }
}