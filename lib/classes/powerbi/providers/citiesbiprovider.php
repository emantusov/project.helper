<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 17:51
 */

namespace Project\Helper\Classes\PowerBI\Providers;


use Bitrix\Iblock\ElementTable,
    \Project\Helper\Tables\Reports\PublicUfCityTable,
    Project\Helper\Classes\PowerBI\Entities\City;

class CitiesBiProvider implements BiProvider
{
    /**
     * @return string
     */
    public static function getQuery()
    {
        return PublicUfCityTable::class;
    }

    /**
     * @param array $dateFilter
     * @return array
     */
    public static function getEntities($dateFilter = [])
    {
        $idIbCities = 43;
        $arCities = [];
        $filter = [
            '=IBLOCK_ID' => $idIbCities,
        ];
        if (!empty($dateFilter)) {
            $filter['><TIMESTAMP_X'] = $dateFilter;
        }
        $select = ['ID', 'NAME', 'CODE'];
        $obCities = ElementTable::query()->setFilter($filter)->setSelect($select)->exec();
        while ($city = $obCities->fetch()) {
            $arCities[] = new City($city);
        }
        return $arCities;
    }

    public static function getCount()
    {
        $arCities = ElementTable::query()->exec()->fetchAll();
        return count($arCities);
    }

    public static function getEntitiesByOffset($offSet)
    {
        $idIbCities = 43;
        $arCities = [];
        $filter = [
            '=IBLOCK_ID' => $idIbCities,
        ];
        $select = ['ID', 'NAME', 'CODE'];
        $obCities = ElementTable::query()->setFilter($filter)->
        setLimit(1000)->setOffset($offSet)->setSelect($select)->exec();
        while ($city = $obCities->fetch()) {
            $arCities[] = new City($city);
        }
        return $arCities;
    }
}