<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 17:51
 */

namespace Project\Helper\Classes\PowerBI\Providers;


use Project\Helper\Tables\Reports\PublicBitrixContactsTable,
    Project\Helper\Classes\PowerBI\Entities\Client,
    Project\Helper\Tables\ContactTable;

class ClientsBiProvider implements BiProvider
{
    /**
     * @return string
     */
    public static function getQuery()
    {
        return PublicBitrixContactsTable::class;
    }

    /**
     * @param array $dateFilter
     * @return array
     */
    public static function getEntities($dateFilter = [])
    {
        $arClients = [];
        $filter = [];
        if (!empty($dateFilter)) {
            $filter['><DATE_MODIFY'] = $dateFilter;
        }
        $select = ['ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'DATE_CREATE', 'CREATED_BY_ID', 'MODIFY_BY_ID',
            'ASSIGNED_BY_ID', 'SOURCE_ID', 'TYPE_ID', 'UF_NEGATIVE', 'UF_CITY', 'UF_BLACK_LIST', 'UF_AMO', 'UF_ROISTAT',
            'UF_RAITING', 'UF_RAITING_CHIT', 'UF_SMS_SUBS', 'UF_MAIL_SUBS', 'UF_SITE', 'UF_UNISENDER', 'UF_PROMO_17',
            'UF_CONTACT_ARCHIV', 'UF_CAMPAIGN', 'UF_SEGMENT', 'UF_ABONEMENT_DATE', 'UF_PASPORT', 'LEAD_ID', 'ADDRESS',
            'COMMENTS', 'SOURCE_DESCRIPTION'];
        $obClients = ContactTable::query()->setFilter($filter)->setSelect($select)->exec();
        while ($client = $obClients->fetch()) {
            if (!$arClients[$client['ID']]) {
                $arClients[$client['ID']] = new Client($client);
            }
        }
        return $arClients;
    }

    /**
     * @param $offSet
     * @return array
     */
    public static function getEntitiesByOffset($offSet)
    {
        $arClients = [];
        $filter = [];
        $select = ['ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'DATE_CREATE', 'CREATED_BY_ID', 'MODIFY_BY_ID',
            'ASSIGNED_BY_ID', 'SOURCE_ID', 'TYPE_ID', 'UF_NEGATIVE', 'UF_CITY', 'UF_BLACK_LIST', 'UF_AMO', 'UF_ROISTAT',
            'UF_RAITING', 'UF_RAITING_CHIT', 'UF_SMS_SUBS', 'UF_MAIL_SUBS', 'UF_SITE', 'UF_UNISENDER', 'UF_PROMO_17',
            'UF_CONTACT_ARCHIV', 'UF_CAMPAIGN', 'UF_SEGMENT', 'UF_ABONEMENT_DATE', 'UF_PASPORT', 'LEAD_ID', 'ADDRESS',
            'COMMENTS', 'SOURCE_DESCRIPTION'];
        $obClients = ContactTable::query()->setFilter($filter)->setSelect($select) ->setOrder(['ID' => 'ASC'])->
           setLimit(1000)->setOffset($offSet)->exec();
        while ($client = $obClients->fetch()) {
            if (!$arClients[$client['ID']]) {
                $arClients[$client['ID']] = new Client($client);
            }
        }
        return $arClients;
    }

    public static function getCount()
    {
        $arContacts = ContactTable::query()->exec()->fetchAll();
        return count($arContacts);
    }
}