<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 17:51
 */

namespace Project\Helper\Classes\PowerBI\Providers;


use
    \Project\Helper\Tables\Reports\BitrixStatusTable,
    Project\Helper\Classes\PowerBI\Entities\Status,
    \Bitrix\Crm\StatusTable;

class StatusBiProvider implements BiProvider
{
    private static $sourceFilter = [
        '=ENTITY_ID' => ['SOURCE', 'DEAL_STAGE', 'DEAL_TYPE', 'CONTACT_TYPE'],
    ];
    private static $select = ['ENTITY_ID', 'STATUS_ID', 'NAME'];
    /**
     * @return string
     */
    public static function getQuery()
    {
        return BitrixStatusTable::class;
    }

    /**
     * @param array $dateFilter
     * @return array
     */
    public static function getEntities($dateFilter = [])
    {
        $arStatus = [];
        $obStatus = StatusTable::query()->setFilter(static::$sourceFilter)->setSelect(static::$select)->exec();
        while ($status = $obStatus->fetch()) {
            $arStatus[] = new Status($status);
        }
        return $arStatus;
    }

    public static function getCount()
    {
        $arStatus = StatusTable::query()->setFilter(static::$sourceFilter)->exec()->fetchAll();
        return count($arStatus);

    }

    /**
     * @param $offSet
     * @return array
     */
    public static function getEntitiesByOffset($offSet)
    {
        $arStatus = [];
        $obStatus = StatusTable::query()->setFilter(static::$sourceFilter)->setSelect(static::$select)->
        setOrder(['ID' => 'ASC'])->setLimit(1000)->setOffset($offSet)->exec();
        while ($status = $obStatus->fetch()) {
            $arStatus[] = new Status($status);
        }
        return $arStatus;
    }
}