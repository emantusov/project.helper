<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 17:51
 */

namespace Project\Helper\Classes\PowerBI\Providers;


use Bitrix\Tasks\TaskTable,
    Project\Helper\Tables\Reports\PublicSalesTable,
    Project\Helper\Classes\PowerBI\Entities\Sale;

class SalesBiProvider implements BiProvider
{
    private static $filter = [
        '=ZOMBIE' => 'N',
        '=UF_TYPE' => ['SERVICES', 'SALE', '1', '2', '3', '4', '9', '14', '15', '31', '32', '33', '34', '35', '36', '37',
            '38', '39', '40', '41', '42'],
    ];
    private static $select = ['ID', 'UF_CRM_TASK', 'DEADLINE', 'CREATED_DATE', 'RESPONSIBLE_ID', 'UF_OPERATOR', 'UF_CITY',
        'UF_TYPE', 'UF_STAGE_ID', 'UF_OPPORTUNITY', 'UF_NO_PREPAYMANTS', 'UF_HOW_WAS_IT', 'UF_REACTION', 'UF_ZONE',
        'UF_RESULT_CONS', 'UF_DATE_PAY', 'UF_DATE_SURCHARGE_N', 'UF_REFUND_DATE', 'UF_DATE_SURCHARGE_B', 'UF_SUM_N',
        'UF_SURCHARGE_B', 'UF_SUM_B', 'UF_REFUND_SUM_N', 'UF_SURCHARGE_N', 'UF_SOURCE', 'UF_REFUND_SUM_B',
        'UF_OLD_DEAL', 'UF_SEGMENT', 'UF_SITE', 'UF_SUM_R', 'UF_DATE_SURCHARGE_B3', 'UF_DATE_SURCHARGE_N3',
        'UF_DATE_SURCHARGE_N2', 'UF_SURCHARGE_N3', 'UF_SURCHARGE_R', 'UF_DATE_SURCHARGE_B2', 'UF_DATE_SURCHARGE_R',
        'UF_SURCHARGE_B3', 'UF_SURCHARGE_N2', 'UF_SURCHARGE_B2', 'UF_LANDING', 'UF_RAITING', 'UF_CHANGE_STUDIO'];

    /**
     * @return string
     */
    public static function getQuery()
    {
        return PublicSalesTable::class;
    }

    /**
     * @param array $dateFilter
     * @return array
     */
    public static function getEntities($dateFilter = [])
    {
        $arSales = [];
        if (!empty($dateFilter)) {
            static::$filter['><CHANGED_DATE'] = $dateFilter;
        }

        $obSales = TaskTable::query()->setFilter(static::$filter)->setSelect(static::$select)->setOrder(['ID' => 'ASC'])->exec();
        while ($sale = $obSales->fetch()) {
            $arSales[] = new Sale($sale);
        }
        return $arSales;
    }

    public static function getCount()
    {
        $arSales = TaskTable::query()->setFilter(static::$filter)->exec()->fetchAll();
        return count($arSales);

    }

    /**
     * @param $offSet
     * @return array
     */
    public static function getEntitiesByOffset($offSet)
    {
        $arSales = [];
        $obSales = TaskTable::query()->setFilter(static::$filter)->setSelect(static::$select)->setOrder(['ID' => 'ASC'])->
        setLimit(1000)->setOffset($offSet)->exec();
        while ($sale = $obSales->fetch()) {
            $arSales[] = new Sale($sale);
        }
        return $arSales;
    }
}