<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 17:51
 */

namespace Project\Helper\Classes\PowerBI\Providers;


use Bitrix\Iblock\ElementTable,
    Project\Helper\Tables\Reports\PublicBitrixSkusTable as SkusTable,
    Project\Helper\Classes\PowerBI\Entities\Zone;

class ZonesBiProvider implements BiProvider
{

    /**
     * @return string
     */
    public static function getQuery()
    {
        return SkusTable::class;
    }

    /**
     * @param array $dateFilter
     * @return array
     */
    public static function getEntities($dateFilter = [])
    {
        $idIbZones = 25;
        $arZones = [];
        $filter = [
            'IBLOCK_ID' => $idIbZones,
        ];
        if(!empty($dateFilter)){
            $filter['><TIMESTAMP_X'] = $dateFilter;
        }

        $select = ['ID', 'NAME',];
        $obZones = ElementTable::query()->setFilter($filter)->setSelect($select)->exec();
        while ($zone = $obZones->fetch()) {
            $arZones[] = new Zone($zone);
        }
        return $arZones;
    }
}