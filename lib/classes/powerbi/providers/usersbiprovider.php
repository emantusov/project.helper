<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 17:51
 */

namespace Project\Helper\Classes\PowerBI\Providers;


use Bitrix\Main\UserTable,
    Project\Helper\Tables\Reports\PublicBitrixUsersTable,
    Project\Helper\Classes\PowerBI\Entities\User;

class UsersBiProvider implements BiProvider
{

    /**
     * @return string
     */
    public static function getQuery()
    {
        return PublicBitrixUsersTable::class;
    }

    /**
     * @param array $dateFilter
     * @return array
     */
    public static function getEntities($dateFilter = [])
    {
        $arUsers = [];
        $filter = [];
        if(!empty($dateFilter)){
            $filter['><TIMESTAMP_X'] = $dateFilter;
        }
        $userSelect = ['ID', 'PERSONAL_PHONE', 'PERSONAL_MAILBOX', 'UF_PHONE_INNER', 'NAME', 'LAST_NAME'];
        $obUsers = UserTable::query()->setFilter($filter)->setSelect($userSelect)->exec();
        while ($user = $obUsers->fetch()){
            $arUsers[] = new User($user);
        }
        return $arUsers;
    }


    public static function getEntitiesByOffset($offSet)
    {
        $arUsers = [];
        $filter = [];
        $userSelect = ['ID', 'PERSONAL_PHONE', 'PERSONAL_MAILBOX', 'UF_PHONE_INNER', 'NAME', 'LAST_NAME'];
        $obUsers = UserTable::query()->setFilter($filter)->setSelect($userSelect)->
        setLimit(1000)->setOffset($offSet)->exec();
        while ($user = $obUsers->fetch()){
            $arUsers[] = new User($user);
        }
        return $arUsers;
    }

    public static function getCount()
    {
        $arContacts = UserTable::query()->exec()->fetchAll();
        return count($arContacts);
    }
}