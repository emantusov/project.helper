<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.11.2018
 * Time: 17:48
 */

namespace Project\Helper\Classes\PowerBI\Providers;


interface BiProvider
{
    public static function getQuery();

    /**
     * @param bool $update
     * @return mixed
     */
    public static function getEntities($update = false);
}