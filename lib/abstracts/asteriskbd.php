<?php

namespace Project\Helper\Abstracts;

use Bitrix\Main\Localization\Loc;


Loc::loadMessages(__FILE__);


abstract class AsteriskBd extends ExternalBd
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return '';
    }

    /**
     * @return string
     */
    public static function getConnectionName()
    {
        return 'asterisk';
    }

    /**
     * @return string
     */
    public static function getCacheDirName()
    {
        return 'asteriskDb';
    }

}