<?php

namespace Project\Helper\Abstracts;

use Bitrix\Main\Localization\Loc;


Loc::loadMessages(__FILE__);


abstract class ReportsBd extends ExternalBd
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return '';
    }

    /**
     * @return string
     */
    public static function getConnectionName()
    {
        return 'reports';
    }

    /**
     * @return string
     */
    public static function getCacheDirName()
    {
        return 'reportsBd';
    }

}