<?php

namespace Project\Helper\Abstracts;

use Bitrix\Main,
    Bitrix\Main\Application,
    Bitrix\Main\Localization\Loc;
use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\DB\SqlQueryException;
use Bitrix\Main\Entity\DataManager;


Loc::loadMessages(__FILE__);


abstract class ExternalBd extends DataManager
{

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return '';
    }

    /**
     * @return string
     */
    public static function getCacheDirName()
    {
        return '';
    }

    /**
     * @return string
     */
    public static function getConnectionName()
    {
        return 'default';
    }


    /**
     * Returns entity map definition.
     * @return array
     */
    public static function getMap()
    {
        try {
            return static::getMapArray();
        } catch (ArgumentNullException $e) {
            die($e->getMessage());
        }catch (SqlQueryException $e){
            die($e->getMessage());
        }
    }

    /**
     * @return array
     * @throws ArgumentNullException
     * @throws Main\Db\SqlQueryException
     */
    private static function getMapArray()
    {
        $arMap = [];
        $connection = Application::getConnection(static::getConnectionName());
        $tableName = static::getTableName();
        if (empty($tableName)) {
            throw new ArgumentNullException('TableName');
        }
        $sql = "DESCRIBE " . $tableName;
        $cacheId = $tableName;
        $cachePath = static::getCacheDirName();
        $cacheTime = 3600;
        $cache = Cache::createInstance();
        if ($cache->initCache($cacheTime, $cacheId, $cachePath)) {
            $arMap = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $resultQuery = $connection->query($sql);
            while ($record = $resultQuery->fetch()) {
                $key = $record['Field'];
                $arMap[$key]['data_type'] = static::getFieldType($record['Type']);
                if ($record['Extra'] === 'auto_increment') {
                    $arMap[$key]['autocomplete'] = true;
                }
                if ($record['Key'] === 'PRI') {
                    $arMap[$key]['primary'] = true;
                }
            }
            $cache->endDataCache($arMap);
        }
        return $arMap;
    }

    /**
     * @param $type
     * @return mixed|string
     */
    private static function getFieldType($type)
    {
        $arTypes = [
            'tinyint' => 'integer',
            'smallint' => 'integer',
            'mediumint' => 'integer',
            'int' => 'integer',
            'bigint' => 'integer',
            'decimal' => 'integer',
            'float' => 'float',
            'double' => 'float',
            'char' => 'string',
            'varchar' => 'string',
            'tinytext' => 'string',
            'text' => 'string',
            'mediumtext' => 'string',
            'longtext' => 'string',
            'timestamp' => 'datetime',
            'date' => 'datetime',
            'datetime' => 'datetime',
        ];
        foreach ($arTypes as $key => $value) {

            if (strpos($type, $key) !== false) {
                return $value;
            }
        }
        return 'string';
    }
}