<?php

namespace Project\Helper;

use Bitrix\Main\Application;
use Bitrix\Main\Data;
use Project\Helper\Interfaces\ICache;

/**
 * Class Cache
 * @package Rebrow\Booking
 */
class Cache extends Data\Cache implements ICache
{
    /**
     * @var string
     */
    protected $initDir;

    /**
     * @param string $relativePath
     * @return $this
     */
    public static function getInstance($relativePath = null)
    {
        static $instance;
        if (empty($instance)) $instance = static::createInstance($relativePath);
        return $instance;
    }

    /**
     * @param string $relativePath
     * @return $this
     */
    public static function createInstance($relativePath = null)
    {
        $cacheEngine = static::createCacheEngine();
        return new static($cacheEngine, is_null($relativePath) ? get_called_class() : $relativePath);
    }

    /**
     * Cache constructor.
     * @param Data\ICacheEngine $cacheEngine
     * @param string $relativePath
     */
    public function __construct(Data\ICacheEngine $cacheEngine, $relativePath)
    {
        $this->initDir = $this->unique($relativePath);
        parent::__construct($cacheEngine);
    }

    /**
     * @param mixed $data
     * @return string
     */
    public function unique($data)
    {
        if (is_array($data)) array_multisort($data, SORT_ASC, SORT_REGULAR);
        return md5(serialize($data));
    }

    /**
     * @param mixed $key
     * @param int $ttl
     * @param callable $callback
     * @return mixed
     */
    public function get($key, $ttl = 3600, $callback = null)
    {
        $result = null;
        if ($this->initCache((int) $ttl, $this->unique($key), $this->initDir)) {
            $result = $this->GetVars();
        } elseif (is_callable($callback) && $this->startDataCache()) {
            $this->startTagCache();
            $result = call_user_func($callback, $this);
            $this->endTagCache()->endDataCache($result);
        }
        return $result;
    }

    /**
     * @param mixed $key
     * @param mixed $data
     * @param int $ttl
     * @param string|array $tags
     * @return $this
     */
    public function set($key, $data, $ttl, $tags = null)
    {
        if ($this->startDataCache((int) $ttl, $this->unique($key), $this->initDir)) {
            if (!empty($tags)) {
                $this->startTagCache();
                if (!is_array($tags)) $tags = [$tags];
                foreach (array_unique($tags) as $tag) $this->registerTag($this->unique($tag));
                $this->endTagCache();
            }
            $this->endDataCache($data);
        }
        return $this;
    }

    /**
     * @param mixed $key
     * @param int $ttl
     * @return bool
     */
    public function start($key, $ttl = 3600)
    {
        if ($this->startDataCache((int) $ttl, $this->unique($key), $this->initDir)) {
            $this->startTagCache();
            return true;
        }
        return false;
    }

    /**
     * @param mixed $vars
     * @return $this
     */
    public function end($vars = false)
    {
        $this->endTagCache()->endDataCache($vars);
        return $this;
    }

    /**
     * @return Data\TaggedCache
     */
    public function getTaggedCache()
    {
        return Application::getInstance()->getTaggedCache();
    }

    /**
     * @param string $tag
     * @return $this
     */
    public function clearByTag($tag)
    {
        $this->getTaggedCache()->clearByTag($this->unique($tag));
        return $this;
    }

    /**
     * @param string $tag
     * @return $this
     */
    public function registerTag($tag)
    {
        $this->getTaggedCache()->registerTag($this->unique($tag));
        return $this;
    }

    /**
     * @return $this
     */
    public function abortTagCache()
    {
        $this->getTaggedCache()->abortTagCache();
        return $this;
    }

    /**
     * @return $this
     */
    public function endTagCache()
    {
        $this->getTaggedCache()->endTagCache();
        return $this;
    }

    /**
     * @return $this
     */
    public function startTagCache()
    {
        $this->getTaggedCache()->startTagCache($this->initDir);
        return $this;
    }
}
