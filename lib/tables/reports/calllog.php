<?php

namespace Project\Helper\Tables\Reports;

use Bitrix\Main\Localization\Loc,
    Project\Helper\Abstracts\ReportsBd;

Loc::loadMessages(__FILE__);


class CallLogTable extends ReportsBd
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'call_logs';
    }
}