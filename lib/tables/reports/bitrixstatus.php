<?php

namespace Project\Helper\Tables\Reports;

use Bitrix\Main\Localization\Loc,
    Project\Helper\Abstracts\ReportsBd;

Loc::loadMessages(__FILE__);


class BitrixStatusTable extends ReportsBd
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'bitrix_status';
    }
}