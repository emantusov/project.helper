<?php

namespace Project\Helper\Tables\Asterisk;

use Bitrix\Main\Localization\Loc,
    Project\Helper\Abstracts\AsteriskBd;

Loc::loadMessages(__FILE__);


class CallLogTable extends AsteriskBd
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'call_log';
    }

    public static function getConnectionName()
    {
        return 'asterisk';
    }
}