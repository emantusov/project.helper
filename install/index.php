<?

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

Class Project_Helper extends CModule
{
	const MODULE_ID = 'project.helper';
	public $MODULE_ID = 'project.helper';
	public $MODULE_VERSION;
	public $MODULE_VERSION_DATE;
	public $MODULE_NAME;
	public $MODULE_DESCRIPTION;


	function __construct()
	{
		$arModuleVersion = array();
		include(__DIR__ . "/version.php");

		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = Loc::getMessage("PROJECT_HELPER_MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("PROJECT_HELPER_MODULE_DESC");

		$this->PARTNER_NAME = Loc::getMessage("PROJECT_HELPER_PARTNER_NAME");
		$this->PARTNER_URI = Loc::getMessage("PROJECT_HELPER_PARTNER_URI");

		$this->MODULE_SORT = 1;
		$this->SHOW_SUPER_ADMIN_GROUP_RIGHTS = 'Y';
		$this->MODULE_GROUP_RIGHTS = "Y";
	}


	function InstallDB($arParams = array())
	{
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		return true;
	}

	function UnInstallFiles()
	{
		return true;
	}

	function DoInstall()
	{
		RegisterModule(self::MODULE_ID);
		$this->InstallFiles();
		$this->InstallDB();

	}

	function DoUninstall()
	{
		$this->UnInstallDB();
		$this->UnInstallFiles();
		UnRegisterModule(self::MODULE_ID);

	}
}